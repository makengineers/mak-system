package com.service.safeservice

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bosphere.filelogger.FL
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Method
import com.google.gson.GsonBuilder
import com.service.mak_system.Key
import com.service.mak_system.RegisterError
import com.service.safeservice.Common.AndroidNetworkUtils
import kotlinx.android.synthetic.main.activity_register.view.*
import java.io.File


class RegisterActivity : AppCompatActivity() {
    private lateinit var registerURL : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        registerURL = baseContext.resources.getString(R.string.register_url)
    }

    fun registerClicked(view: View) {
        val usernameEditor = findViewById<EditText>(R.id.username)
        val username = usernameEditor.text.toString()

        val passwordEditor = findViewById<EditText>(R.id.password)
        val password = passwordEditor.text.toString()

        val confirmPasswordEditor = findViewById<EditText>(R.id.confirmPassword)
        val confirmPassword = confirmPasswordEditor.text.toString()

        if (password != confirmPassword) {
            confirmPasswordEditor.error = "Passwords do not match"
            return
        }

        if(!canSend()){
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setMessage("No internet connection!")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
            return
        }

        val progressBar = findViewById<ProgressBar>(R.id.registerProgressBar)
        progressBar.visibility = View.VISIBLE

        val formData : ArrayList<Pair<String, String>> = ArrayList(5)
        formData.add(Pair("username", username))
        formData.add(Pair("password1", password))
        formData.add(Pair("password2", password))

        val resultIntent = Intent()

        Fuel.upload(registerURL, Method.POST, parameters = formData)
            .response { _, response, result ->
                val (bytes, error) = result
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                    if(response.statusCode != 201 && response.statusCode != 400) {
                        runOnUiThread {
                            progressBar.visibility = View.GONE
                            AlertDialog.Builder(this).setMessage("Error").show()
                        }
                        return@response
                    }
                    val gson = GsonBuilder().create()
                    val errorResult = gson.fromJson(String(response.data), RegisterError::class.java)
                    runOnUiThread {
                        if(errorResult == null) {
                            AlertDialog.Builder(this).setMessage("Error").show()
                            return@runOnUiThread
                        }
                        if (errorResult.username != null) {
                            progressBar.visibility = View.GONE
                            usernameEditor.error = errorResult.username.toString().substring(1,errorResult.username.toString().length-1)
                        }
                        if (errorResult.password1 != null) {
                            progressBar.visibility = View.GONE
                            passwordEditor.error = errorResult.password1.toString().substring(1,errorResult.password1.toString().length-1)
                        }
                    }
                }
                if (bytes != null) {
                    val gson = GsonBuilder().create()
                    val keyObject = gson.fromJson(String(bytes), Key::class.java)
                    val key = keyObject.key
                    FL.i("Received device key $key")
                    val file = File(baseContext.filesDir, resources.getString(R.string.received_key_filename))
                    file.writeText(key)

                    resultIntent.putExtra("key", key)
                    setResult(Activity.RESULT_OK, resultIntent)
                    this.finishAndRemoveTask()

                }
            }
    }

    private fun canSend() : Boolean {
        val network = AndroidNetworkUtils(baseContext)
        return network.isMobileConnected() || network.isWifiConnected()
    }
}