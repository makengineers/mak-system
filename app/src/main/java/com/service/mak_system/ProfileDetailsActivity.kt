package com.service.safeservice

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bosphere.filelogger.FL
import com.google.gson.Gson
import com.service.mak_system.ProfileInfo
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class ProfileDetailsActivity : AppCompatActivity() {
    private val dateFormat : DateFormat
    private val niceDateFormat : DateFormat

    init {
        dateFormat = try {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX")
        } catch (e : IllegalArgumentException) {
            FL.e(e.message)
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        }
        niceDateFormat = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_details)

        val path = intent.getStringExtra("filepath") as String

        val file = File(path)
        val profileInfo = Gson().fromJson(file.readText(), ProfileInfo::class.java)

        val creationDate = findViewById<TextView>(R.id.creation_date_value)
        val score = findViewById<TextView>(R.id.score_value)
        val usedClassSamples = findViewById<TextView>(R.id.used_class_samples_value)
        val precision = findViewById<TextView>(R.id.precision_value)
        val recall = findViewById<TextView>(R.id.recall_value)
        val fscore = findViewById<TextView>(R.id.fscore_value)

        val creationDateDate = dateFormat.parse(profileInfo.creation_date as String)
        niceDateFormat.timeZone = TimeZone.getTimeZone("Poland")
        val formatedDate = niceDateFormat.format(creationDateDate as Date)

        creationDate.text = formatedDate
        if(profileInfo.score!= null) {
            score.text = ((profileInfo.score.toFloat() * 100 * 100).roundToInt() / 100F).toString() + "%"
        }
        if(profileInfo.precision!= null) {
            precision.text = ((profileInfo.precision.toFloat() * 100 * 100).roundToInt() / 100F).toString() + "%"
        }
        if(profileInfo.recall!= null) {
            recall.text = ((profileInfo.recall.toFloat() * 100 * 100).roundToInt() / 100F).toString() + "%"
        }
        if(profileInfo.fscore!= null) {
            fscore.text = ((profileInfo.fscore.toFloat() * 100 * 100).roundToInt() / 100F).toString() + "%"
        }
        usedClassSamples.text = profileInfo.used_class_samples.toString()
    }

    fun okClicked(view: View){
        this.finishAndRemoveTask()
    }
}