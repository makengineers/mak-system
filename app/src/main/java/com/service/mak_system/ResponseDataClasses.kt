package com.service.mak_system

data class Device(val id: Int, val android_id: String)

data class Key(val key: String)

data class RegisterError(val username: ArrayList<String>?, val password1: ArrayList<String>?,
                         val non_field_errors: ArrayList<String>?,
                         val detail: ArrayList<String>?)

data class LoginError(val username: ArrayList<String>?, val password: ArrayList<String>?,
                         val non_field_errors: ArrayList<String>?, val detail: ArrayList<String>?)

data class DataPostApiResponse(val id : Int?, val device: String?, val file_type: String?, val start_date : String?, val data : String?,
                               val non_field_errors: ArrayList<String>?, val detail: ArrayList<String>?)

data class LatestProfile(val id : Int?, val creation_date : String?)

data class ProfileInfo(val creation_date : String?, val used_class_samples:  Int?, val score : Float?,
                       val precision : Float?, val recall : Float?, val fscore : Float?)

data class GetError(val detail : String?)