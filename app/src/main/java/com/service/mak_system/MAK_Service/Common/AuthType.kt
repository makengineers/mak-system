package com.service.safeservice.Common

enum class AuthType {
    NORMAL,
    CONTINUOUS
}