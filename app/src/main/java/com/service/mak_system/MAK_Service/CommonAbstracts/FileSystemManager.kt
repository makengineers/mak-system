package com.service.safeservice.CommonAbstracts

import java.io.File
import java.io.FileOutputStream

abstract class FileSystemManager(private val fileDir: File) {

    abstract fun OpenFile(filename: String) : FileOutputStream

    abstract fun LoadFile(filename: String) : File

    abstract fun ListFiles() : MutableList<File>

    abstract fun DeleteFile(filename: String) : Boolean

    abstract fun CreateDirectory(name: String) : Boolean

    abstract fun ChangeWorkingDirectory(name: String) : Boolean

    abstract fun ContainsFile(filename: String) : Boolean

}