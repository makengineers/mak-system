package com.service.safeservice

import android.R.drawable
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.bosphere.filelogger.FL
import com.bosphere.filelogger.FLConfig
import com.bosphere.filelogger.FLConst
import com.google.gson.Gson
import com.service.mak_system.Device
import com.service.mak_system.MAK_Service.Architecture.ChaquopyArchitectureHelper
import com.service.mak_system.MAK_Service.CommonAbstracts.ArchitectureHelper
import com.service.safeservice.Auth.AndroidAuthProvider
import com.service.safeservice.Auth.AndroidSwitchDetector
import com.service.safeservice.Common.AndroidInternalFileSystemManager
import com.service.safeservice.Common.AndroidNetworkUtils
import com.service.safeservice.Common.BiometricAuthenticationAllower
import com.service.safeservice.CommonAbstracts.*
import com.service.safeservice.CommonAbstracts.Observable
import com.service.safeservice.Configs.AndroidUserConfigProvider
import com.service.safeservice.ContinuousAuth.AndroidContinuousAuthProvider
import com.service.safeservice.ContinuousAuth.ContinuousAuthSwitchDetector
import com.service.safeservice.Events.AndroidEventDataGatherer
import com.service.safeservice.Events.AndroidEventDataSaver
import com.service.safeservice.Senders.AndroidApiRequester
import com.service.safeservice.Senders.AndroidDataSender
import com.service.safeservice.Sensors.AndroidSensorDataConverter
import com.service.safeservice.Sensors.AndroidSensorDataGatherer
import com.service.safeservice.Sensors.AndroidSensorDataSaver
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.exitProcess


class MainService : Service() {
    private val screenOffReceiver: ScreenOffBroadcastReceiver = ScreenOffBroadcastReceiver()
    private lateinit var suicidalBroadcastReceiver: SuicidalBroadcastReceiver
    private val shutdownBroadcastReceiver: ShutdownBroadcastReceiver = ShutdownBroadcastReceiver(this)
    var counter = 0
    private val dateFormat = SimpleDateFormat("yyyy.MM.dd'T'HH-mm-ss'Z'")
    private lateinit var sensorDataGatherer: SensorDataGatherer
    private lateinit var eventDataGatherer: EventDataGatherer
    private lateinit var sensorSaver: SensorDataSaver
    private lateinit var eventSaver: EventDataSaver
    private var deviceId: Int = -1
    private var key: String = ""
    private lateinit var dataSender: DataSender
    private lateinit var sensorManager: SensorManager
    private lateinit var normalAuthProvider: SensorAuthProvider
    private lateinit var continuousAuthProvider: SensorAuthProvider
    private lateinit var unlockSwitchDetector: SwitchDetector
    private lateinit var continuousAuthSwitchDetector: SwitchDetector
    private lateinit var networkUtils: NetworkUtils
    private lateinit var userConfigProvider: UserConfigProvider
    private lateinit var biometricAuthAllower: BiometricAuthenticationAllower
    private lateinit var architectureHelper: ArchitectureHelper

    private class SimpleSensorListener(private val service: MainService) : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            service.sensorDataGatherer.ProcessAccuracyChange(sensor, accuracy)
        }

        override fun onSensorChanged(event: SensorEvent?) {
            service.sensorDataGatherer.ProcessSensorEvent(event)
        }

    }

    private class EventBroadcastReceiver(private val service: MainService) : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            service.eventDataGatherer.ProcessEvent(context, intent)
        }

    }

    private class SuicidalBroadcastReceiver(private val service: MainService) : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            service.shutdown()
            exitProcess(0)
        }
    }

    private class EventBroadcaster: Observable<String>() {
        fun Process(str: String) {
            Notify(str)
        }
    }

    private val eventBroadcaster = EventBroadcaster()
    private val sensorListener: SimpleSensorListener = SimpleSensorListener(this)
    var sensors: ArrayList<Sensor> = ArrayList(6)
    private  val eventListener: EventBroadcastReceiver = EventBroadcastReceiver(this)

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        try {

            super.onCreate()

            val devicesFile = File(baseContext.filesDir, resources.getString(R.string.device_id_json_filename))
            if(!devicesFile.exists()){
                return
            }
            else {
                val keyFile = File(baseContext.filesDir, resources.getString(R.string.received_key_filename))
                if (!keyFile.exists()) {
                    return
                }
                else {
                    val device = Gson().fromJson(devicesFile.readText(), Device::class.java)
                    deviceId = device.id
                    key = keyFile.readText()
                }
            }

            FL.init(
                FLConfig.Builder(this)
                    .minLevel(FLConst.Level.V)
                    .logToFile(true)
                    .dir(File(baseContext.getExternalFilesDir(null), resources.getString(R.string.external_log_filename)))
                    .retentionPolicy(FLConst.RetentionPolicy.TOTAL_SIZE)
                    .maxTotalSize(1024*1024*10)
                    .build())
            FL.setEnabled(true)

            FL.i("Create service on " + dateFormat.format(Date()))

            makeNiceDirectories()

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
                startMyOwnForeground()
            else
                startMyOwnForegroundLegacy()

            //Loading globals from context
            userConfigProvider = AndroidUserConfigProvider(filesDir, resources.getString(R.string.user_config_filename))
            biometricAuthAllower = BiometricAuthenticationAllower(
                numberOfAttemptsAllows = true,
                reasonBehindLockAllows = true
            )

            architectureHelper = ChaquopyArchitectureHelper()
            val architecture = architectureHelper.GetPythonArchitecture()

            networkUtils = AndroidNetworkUtils(baseContext)

            sensorDataGatherer = AndroidSensorDataGatherer(resources.getString(R.string.sensor_gatherer_interval_sec).toFloat(),
                resources.getInteger(R.integer.sensor_gatherer_max_storage_size))

            eventDataGatherer = AndroidEventDataGatherer()

            val sensorfilesystemManager = AndroidInternalFileSystemManager(filesDir)
            val eventfilesystemManager = AndroidInternalFileSystemManager(filesDir)
            val converter = AndroidSensorDataConverter(
                resources.getString(R.string.max_sensor_acceleration_value).toFloat(),
                resources.getString(R.string.max_sensor_magnetic_field_value).toFloat(),
                resources.getString(R.string.max_sensor_gyroscope_value).toFloat(),
                resources.getString(R.string.max_sensor_gravity_value).toFloat(),
                resources.getString(R.string.max_sensor_linear_acceleration_value).toFloat(),
                resources.getString(R.string.max_sensor_rotation_value).toFloat())

            sensorSaver = AndroidSensorDataSaver(sensorDataGatherer, eventBroadcaster, sensorfilesystemManager,
                resources.getInteger(R.integer.max_sensor_file_size_bytes).toLong(), converter)

            eventSaver = AndroidEventDataSaver(eventDataGatherer, eventBroadcaster, eventfilesystemManager,
                resources.getInteger(R.integer.max_events_file_size_bytes).toLong())

        //Data collecting
        val eventFilters = IntentFilter()
        eventFilters.addAction(Intent.ACTION_SCREEN_ON)
        eventFilters.addAction(Intent.ACTION_SCREEN_OFF)
        eventFilters.addAction(Intent.ACTION_USER_PRESENT)
        eventFilters.addAction("ACTION_UNLOCKED")
        eventFilters.addAction("ACTION_UNLOCKED_BIOMETRICALLY")

        registerReceiver(eventListener, eventFilters)
        registerShutdownReceiver()
        registerScreenOffReceiver()
        registerSuicidalReceiver()

            sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
            sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER))
            sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD))
            sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE))
            sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY))
            sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION))
            sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR))

            for (sensor in sensors) {
                sensorManager.registerListener(sensorListener, sensor, SensorManager.SENSOR_DELAY_FASTEST)
            }

            val apiRequester = AndroidApiRequester(filesDir, deviceId, key,
                resources.getString(R.string.post_data_url),
                resources.getString(R.string.devices_url),
                resources.getString(R.string.profiles_url),
                architecture)

            dataSender = AndroidDataSender(
                sensorSaver,
                eventSaver,
                apiRequester,
                networkUtils,
                userConfigProvider,
                baseContext,
                resources.getInteger(R.integer.data_send_interval_ms).toLong()
            )

            dataSender.Run()

            var toastHandler = Handler()

            normalAuthProvider = AndroidAuthProvider(baseContext.filesDir, apiRequester,
                networkUtils, userConfigProvider, toastHandler, baseContext,
                resources.getInteger(R.integer.auth_profile_request_interval_ms).toLong(),
                resources.getString(R.string.normal_profile_filename))

            normalAuthProvider.Run()

            unlockSwitchDetector = AndroidSwitchDetector(sensorDataGatherer, normalAuthProvider, baseContext, biometricAuthAllower,
                userConfigProvider,resources.getInteger(R.integer.screen_on_auth_delay_ms).toLong(), resources.getInteger(R.integer.auth_total_period_ms))

            unlockSwitchDetector.Run()

            continuousAuthProvider =
                AndroidContinuousAuthProvider(
                    baseContext.filesDir,
                    apiRequester,
                    networkUtils,
                    userConfigProvider,
                    resources.getInteger(R.integer.continuous_auth_profile_request_interval_ms).toLong(),
                    resources.getString(R.string.continuous_profile_filename)
                )

            continuousAuthProvider.Run()

            continuousAuthSwitchDetector = ContinuousAuthSwitchDetector(sensorDataGatherer,
                continuousAuthProvider, baseContext, userConfigProvider, biometricAuthAllower,
                resources.getInteger(R.integer.continuous_auth_event_delay_ms),
                resources.getInteger(R.integer.continuous_auth_max_failures),
                resources.getInteger(R.integer.continuous_auth_failure_reset))

            continuousAuthSwitchDetector.Run()

        }
        catch (e: Exception){
            FL.e(e, "MainService failed: ")
            throw e
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun startMyOwnForeground() {
        val NOTIFICATION_CHANNEL_ID = "example.permanence"
        val channelName = "Background Service"
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(chan)

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(drawable.ic_secure)
            .setContentTitle("App is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(2, notification)
    }

    private fun startMyOwnForegroundLegacy() {
        val notificationBuilder = NotificationCompat.Builder(this, "com.service.safeservice")
        val notification =  notificationBuilder.setOngoing(true)
            .setSmallIcon(drawable.ic_secure)
            .setContentTitle("App is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(1, notification)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        startTimer()
        return START_NOT_STICKY
    }

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private fun startTimer() {
        timer = Timer()
        timerTask = object : TimerTask() {
            override fun run() {
                Log.i("Count", "=========  " + counter++)
            }
        }
        timer!!.schedule(timerTask, 0, 10000)
    }

    private fun stopTimerTask() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(screenOffReceiver)
            unregisterReceiver(shutdownBroadcastReceiver)
            unregisterReceiver(suicidalBroadcastReceiver)
            unregisterReceiver(unlockSwitchDetector as BroadcastReceiver)
            unregisterReceiver(continuousAuthSwitchDetector as BroadcastReceiver)
            unregisterReceiver(eventListener)
        }
        catch(e : IllegalArgumentException) {
            FL.e(e, "Failed to unregister receiver: ")
        }

        stopTimerTask()

        val broadcastIntent = Intent()
        broadcastIntent.action = "RestartService"
        broadcastIntent.setClass(this, Restarter::class.java)
        this.sendBroadcast(broadcastIntent)
        super.onDestroy()
    }

    private fun registerShutdownReceiver() {
        val eventFilters = IntentFilter()
        eventFilters.addAction(Intent.ACTION_SHUTDOWN)
        eventFilters.addAction(Intent.ACTION_REBOOT)
        registerReceiver(shutdownBroadcastReceiver, eventFilters)
    }

    private fun registerScreenOffReceiver() {
        val screenStateFilter = IntentFilter(Intent.ACTION_SCREEN_OFF)
        registerReceiver(screenOffReceiver, screenStateFilter)
    }

    private fun registerSuicidalReceiver() {
        suicidalBroadcastReceiver = SuicidalBroadcastReceiver(this)
        val filter = IntentFilter("KILL_YOURSELF")
        registerReceiver(suicidalBroadcastReceiver, filter)
    }

    fun shutdown() {
        eventBroadcaster.Process("shutdown")
    }

    private fun makeNiceDirectories(){
        val sensorDir = File(filesDir, "SensorData")
        if(!sensorDir.exists()) {
            sensorDir.mkdir()
        }
        val eventDir = File(filesDir, "EventData")
        if(!eventDir.exists()) {
            eventDir.mkdir()
        }

        val fileList : MutableList<File> = mutableListOf()
        if(filesDir.listFiles()!=null) {
            filesDir.listFiles().forEach {
                run {
                    if(it.isFile)
                        fileList.add(it)
                }
            }
        }

        fileList.forEach {
            run {
                if(it.name.startsWith("Sensors")){
                    val file = File(sensorDir, it.name)
                    file.writeBytes(File(filesDir, it.name).readBytes())
                    File(filesDir, it.name).delete()
                }
                if (it.name.startsWith("Events")) {
                    val file = File(eventDir, it.name)
                    file.writeBytes(File(filesDir, it.name).readBytes())
                    File(filesDir, it.name).delete()
                }
            }
        }
    }
}