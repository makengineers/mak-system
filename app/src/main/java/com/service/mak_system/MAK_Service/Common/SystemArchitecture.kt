package com.service.mak_system.MAK_Service.Common

enum class SystemArchitecture {
    BIT32,
    BIT64,
    UNKNOWN
}