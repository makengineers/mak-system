package com.service.safeservice.ContinuousAuth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.widget.Toast
import com.service.safeservice.CommonAbstracts.SensorAuthProvider
import com.service.safeservice.CommonAbstracts.SensorDataGatherer
import com.service.safeservice.CommonAbstracts.SwitchDetector
import com.service.safeservice.CommonAbstracts.UserConfigProvider
import com.service.safeservice.Common.BiometricAuthenticationAllower
import com.service.safeservice.ScreenLockRunner
import java.sql.Time
import java.util.*

class ContinuousAuthSwitchDetector(private val sensorDataGatherer: SensorDataGatherer,
     private val authProvider: SensorAuthProvider, private val context: Context,
     private val userConfigProvider: UserConfigProvider, private val biometricAuthAllower : BiometricAuthenticationAllower,
     private val eventsDelay: Int, private val maxFailures: Int, private val failureReset: Int)
    : SwitchDetector, ScreenLockRunner, BroadcastReceiver() {
    private var failuresCounter: Int = 0
    private var successCounter: Int = 0
    private var unlocked: Boolean = false
    private lateinit var task: TimerTask
    private val timer = Timer()
    override fun Run() {
        val eventFilters = IntentFilter()
        eventFilters.addAction("ACTION_PHONE_UNLOCKED")
        eventFilters.addAction("ACTION_UNLOCKED_BIOMETRICALLY")
        eventFilters.addAction("CONFIG_CHANGED")
        eventFilters.addAction(Intent.ACTION_SCREEN_OFF)
        context.registerReceiver(this, eventFilters)

        task = object : TimerTask() {override fun run() {}}
    }


    override fun onReceive(c : Context?, intent: Intent?) {
        unlocked = intent!!.action == "ACTION_PHONE_UNLOCKED" || intent.action == "ACTION_UNLOCKED_BIOMETRICALLY" || intent.action == "CONFIG_CHANGED"
        val userConfig = userConfigProvider.GetCurrentUserConfig() ?: return

        if(userConfig.enableLock) {
            if (unlocked) {
                task = object : TimerTask() {
                    override fun run() {
                        var authResult : Boolean? = null
                        if(userConfig.enableUnlock) {
                            authResult = authProvider.Authorize(sensorDataGatherer.GetLastEventsInTime(eventsDelay)) ?: return
                        }
                        if(authResult == null) return
                        if (authResult) {
                            calcSuccess()
                        } else {
                            calcFailure()
                        }

                        if (shouldLock()) {
                            biometricAuthAllower.ChangeWhetherReasonBehindLockAllows(false)
                            runScreenLock(context, userConfigProvider)
                            this.cancel()
                            resetCounters()
                        }
                    }
                }
                timer.schedule(task, eventsDelay.toLong(), eventsDelay.toLong())
            } else {
                task.cancel()
                resetCounters()
            }
        }
        else {
            task.cancel()
            resetCounters()
        }
    }

    private fun resetCounters()
    {
        successCounter = 0
        failuresCounter = 0
    }

    private fun calcSuccess()
    {
        successCounter += 1
        if(successCounter >= failureReset) {
            successCounter = 0
            failuresCounter = 0
        }
    }

    private fun calcFailure()
    {
        successCounter = 0
        failuresCounter += 1
    }

    private fun shouldLock() : Boolean
    {
        return failuresCounter > maxFailures
    }
}