package com.service.mak_system.MAK_Service.CommonAbstracts

import com.service.mak_system.MAK_Service.Common.SystemArchitecture

interface ArchitectureHelper {
    fun GetPythonArchitecture(): SystemArchitecture
}