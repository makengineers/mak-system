package com.service.safeservice.Common

class BiometricAuthenticationAllower (private var numberOfAttemptsAllows : Boolean, private var reasonBehindLockAllows : Boolean) {

    fun IsAllowed(): Boolean {
        return numberOfAttemptsAllows && reasonBehindLockAllows
    }

    fun ChangeWhetherNumberOfAttemptsAllows(value : Boolean) {
        numberOfAttemptsAllows = value
    }

    fun ChangeWhetherReasonBehindLockAllows(value : Boolean) {
        reasonBehindLockAllows = value
    }
}