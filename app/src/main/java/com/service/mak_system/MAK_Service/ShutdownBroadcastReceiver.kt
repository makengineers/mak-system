package com.service.safeservice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class ShutdownBroadcastReceiver(private val service: MainService) : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if(intent.action.equals(Intent.ACTION_SHUTDOWN) || intent.action.equals(Intent.ACTION_REBOOT)){
            intent.action?.let{ service.shutdown() }
        }
    }
}