package com.service.safeservice.Common

import android.content.Intent
import java.lang.RuntimeException

enum class SystemEventType {
    TOUCH,
    SCREEN_ON,
    SCREEN_OFF,
    USER_PRESENT,
    UNLOCKED_BIOMETRICALLY,
    UNLOCKED
}

class SystemEvent(var timestamp: Long, var type: SystemEventType, var extraData: IntArray) {

    @kotlin.ExperimentalUnsignedTypes
    fun ToBytes(): UByteArray {
        var res = ByteHelper.ToUByteArrayBE(timestamp) + ByteHelper.ToUByteArrayBE(type.ordinal)
        for (i in extraData) {
            res += ByteHelper.ToUByteArrayBE(i)
        }
        return res
    }

    companion object {
        fun ConvertToSystemEventType(type: String) : SystemEventType {
            return when(type) {
                Intent.ACTION_SCREEN_ON -> SystemEventType.SCREEN_ON
                Intent.ACTION_SCREEN_OFF -> SystemEventType.SCREEN_OFF
                Intent.ACTION_USER_PRESENT -> SystemEventType.USER_PRESENT
                "ACTION_UNLOCKED" -> SystemEventType.UNLOCKED
                "ACTION_UNLOCKED_BIOMETRICALLY" -> SystemEventType.UNLOCKED_BIOMETRICALLY
                else -> throw RuntimeException("Failed to parse ${type}")
            }
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromBytes(ar: UByteArray): SystemEvent {
            val timestamp = ByteHelper.FromUByteArrayToLongBE(ar, 0)
            val type = SystemEventType.values()[ByteHelper.FromUByteArrayToIntBE(ar, 8)]
            var extraData =
            if(type == SystemEventType.TOUCH) {
                val extraData = IntArray(2)
                extraData[0] = ByteHelper.FromUByteArrayToIntBE(ar, 12)
                extraData[1] = ByteHelper.FromUByteArrayToIntBE(ar, 16)
                extraData
            }
            else {
                val extraData = IntArray(0)
                extraData
            }
            return SystemEvent(
                timestamp,
                type,
                extraData
            )
        }
    }
}