package com.service.safeservice.CommonAbstracts

import android.content.Context
import java.io.File
import java.util.*

interface ApiRequester {
    fun SendBinaryData(sensorsFilename: String, eventsFilename: String,
                       sensorDate: String, eventDate: String,
                       filePostprocessor: (File) -> Boolean)
    fun RequestProfile(currentProfileCreationDate : Date?, profileFilename: String)
    fun RequestContinuousAuthProfile(currentProfileCreationDate : Date?, profileFilename: String)
}