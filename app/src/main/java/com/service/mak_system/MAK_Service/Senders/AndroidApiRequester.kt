package com.service.safeservice.Senders

import android.content.Context
import com.bosphere.filelogger.FL
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.core.requests.download
import com.google.gson.Gson
import com.service.mak_system.DataPostApiResponse
import com.service.mak_system.GetError
import com.service.mak_system.LatestProfile
import com.service.mak_system.MAK_Service.Common.SystemArchitecture
import com.service.safeservice.CommonAbstracts.ApiRequester
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.ArrayList
import kotlin.concurrent.thread


class AndroidApiRequester(private val filesDir: File, private val deviceId: Int, private val key: String,
                          private val dataPostUrl: String, private val devicesUrl: String,
                          private val profilesUrl: String, private val architecture: SystemArchitecture
)
    : ApiRequester {

    private val mutex = ReentrantLock()
    private val dateFormat : DateFormat

    init {
        dateFormat = try {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX")
        } catch (e : IllegalArgumentException) {
            FL.e(e.message)
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        }
    }

    override fun SendBinaryData(sensorsFilename: String, eventsFilename: String,
                                sensorDate: String, eventDate: String,
                                filePostprocesor: (File) -> Boolean) {
        FL.i("Sending: files: $sensorsFilename, $eventsFilename")
        mutex.lock()
        try {
            FL.i("Sending files")
            val eventFormData : ArrayList<Pair<String, String>> = ArrayList(3)
            eventFormData.add(Pair("device", deviceId.toString()))
            eventFormData.add(Pair("start_date", eventDate))
            eventFormData.add(Pair("file_type", "Event"))

            val eventDir = File(filesDir, "EventData")
            val eventFile = File(eventDir, eventsFilename)

            Fuel.upload(dataPostUrl, parameters = eventFormData)
                .add(FileDataPart(eventFile, name = "data", filename = eventsFilename))
                .timeout(60000)
                .timeoutRead(60000)
                .authentication()
                .bearer(key)
                .response { _, response, result ->
                    val (bytes, error) = result
                    if(error != null) {
                        val errorResult = Gson().fromJson(String(response.data), GetError::class.java)
                        FL.e("Fuel error", error.message.toString())
                        FL.e("Server error", errorResult.detail)
                    }
                    if (bytes != null) {
                        val gson = Gson()

                        val updateResponse = gson.fromJson(String(bytes), DataPostApiResponse::class.java)

                        val log = "Server response received for: $eventsFilename\n\t"

                        if(updateResponse.detail != null) {
                            FL.e("Server error",log + "Message: ${updateResponse.detail}")
                        }
                        else {
                            FL.i("Event file sent successfully!\n\t$log")
                            val eventFilePostprocessed = filePostprocesor(eventFile)
                            FL.i("$eventsFilename removed: $eventFilePostprocessed")
                        }
                    }
                }

            val sensorDir = File(filesDir, "SensorData")
            val sensorFile = File(sensorDir, sensorsFilename)

            val sensorFormData : ArrayList<Pair<String, String>> = ArrayList(3)
            sensorFormData.add(Pair("device", deviceId.toString()))
            sensorFormData.add(Pair("start_date", sensorDate))
            sensorFormData.add(Pair("file_type", "Sensor"))

            Fuel.upload(dataPostUrl, parameters = sensorFormData)
                .add(FileDataPart(sensorFile, name = "data", filename = sensorsFilename))
                .timeout(60000)
                .timeoutRead(60000)
                .authentication()
                .bearer(key)
                .response { _, response, result ->
                    val (bytes, error) = result
                    if(error != null) {
                        val errorResult = Gson().fromJson(String(response.data), GetError::class.java)
                        FL.e("Fuel error", error.message.toString())
                        FL.e("Server error", errorResult.detail)
                    }
                    if (bytes != null) {
                        val gson = Gson()

                        val updateResponse = gson.fromJson(String(bytes), DataPostApiResponse::class.java)

                        val log = "Server response received for: $sensorsFilename\n\t"
                        if(updateResponse.detail != null) {
                            FL.e("Server error",log + "Message: ${updateResponse.detail}")
                        }
                        else {
                            FL.i("Sensor file sent successfully!\n\t$log")
                            val sensorFilePostprocessed = filePostprocesor(sensorFile)
                            FL.i("$sensorsFilename removed: $sensorFilePostprocessed")
                        }
                    }
                }
        }
        finally {
            mutex.unlock()
        }
    }

    override fun RequestProfile(currentProfileCreationDate : Date?, profileFilename : String) {
        FL.i("Behavioral profile request sending...")

        mutex.lock()
        try {
            val formData : ArrayList<Pair<String, String>> = ArrayList(1)
            formData.add(Pair("profile_type", "Unlock"))
            val architectureString: String = (architecture == SystemArchitecture.BIT64).toString().capitalize()
            formData.add(Pair("is_64bit", architectureString))

            val getLatestProfileURL = "${devicesUrl}${deviceId}/profiles/latest"

            thread(start = true) {
                val (_, response, result) = Fuel.get(getLatestProfileURL, parameters = formData)
                    .authentication()
                    .bearer(key)
                    .response()

                val (bytes, error) = result
                if (error != null) {
                    val errorResult = Gson().fromJson(String(response.data), GetError::class.java)
                    FL.e("Fuel error", error.message.toString())
                    FL.e("Server error", errorResult.detail)
                }
                if (bytes != null) {
                    val latestProfile = Gson().fromJson(String(bytes), LatestProfile::class.java)
                    FL.i("Latest profile response received: id ${latestProfile.id}, creation_date ${latestProfile.creation_date}")
                    val newCreationDate = dateFormat.parse(latestProfile.creation_date as String)

                    if (newCreationDate != null) {
                        if (currentProfileCreationDate == null || currentProfileCreationDate < newCreationDate) {

                            val getProfileInfoURL = "${profilesUrl}${latestProfile.id}/info"

                            thread(start = true) {
                                val (_, anotherResponse, anotherResult) = Fuel.get(getProfileInfoURL)
                                    .authentication()
                                    .bearer(key)
                                    .response()

                                val (moreBytes, anotherError) = anotherResult
                                if (anotherError != null) {
                                    val errorResult = Gson().fromJson(
                                        String(anotherResponse.data),
                                        GetError::class.java
                                    )
                                    FL.e("Fuel error", anotherError.message.toString())
                                    FL.e("Server error", errorResult.detail)
                                }
                                if (moreBytes != null) {
                                    val infoFile = File(filesDir, "${profileFilename}Info.json")
                                    infoFile.writeText(String(moreBytes))
                                }
                            }
                            val downloadProfileURL = "${profilesUrl}${latestProfile.id}/file"

                            thread(start = true) {
                                val yetAnotherResult = Fuel.get(downloadProfileURL)
                                    .authentication()
                                    .bearer(key)
                                    .download()
                                    .fileDestination { yetAnotherResponse, url ->
                                        if (yetAnotherResponse.statusCode == 200) {
                                            File(filesDir, "${profileFilename}.joblib")
                                        } else File(filesDir, "${profileFilename}.error")
                                    }
                                    .response()

                                if (yetAnotherResult.component2() == null) {
                                    FL.i("Profile updated")
                                }
                            }
                        } else return@thread
                    }
                }
            }
        } finally {
            mutex.unlock()
        }
    }

    override fun RequestContinuousAuthProfile(currentProfileCreationDate : Date?, profileFilename : String) {
        FL.i("Continuous behavioral profile request sending...")

        mutex.lock()
        try {
            val formData : ArrayList<Pair<String, String>> = ArrayList(1)
            formData.add(Pair("profile_type", "Continuous"))
            val architectureString: String = (architecture == SystemArchitecture.BIT64).toString().capitalize()
            formData.add(Pair("is_64bit", architectureString))

            val getLatestProfileURL = "${devicesUrl}$deviceId/profiles/latest"

            thread(start = true) {
                val (_, response, result) = Fuel.get(getLatestProfileURL, parameters = formData)
                    .authentication()
                    .bearer(key)
                    .response ()

                val (bytes, error) = result
                if (error != null) {
                    val errorResult = Gson().fromJson(String(response.data), GetError::class.java)
                    FL.e("Fuel error", error.message.toString())
                    FL.e("Server error", errorResult.detail)
                }
                if (bytes != null) {
                    val latestProfile = Gson().fromJson(String(bytes), LatestProfile::class.java)
                    FL.i("Latest profile response received: id ${latestProfile.id}, creation_date ${latestProfile.creation_date}")

                    val newCreationDate =
                        dateFormat.parse(latestProfile.creation_date as String)

                    if (newCreationDate != null) {
                        if (currentProfileCreationDate == null || currentProfileCreationDate < newCreationDate) {

                            val getProfileURL = "${profilesUrl}${latestProfile.id}/info"
                            thread(start = true) {
                                val (_, anotherResponse, anotherResult) = Fuel.get(getProfileURL)
                                    .authentication()
                                    .bearer(key)
                                    .response()

                                val (moreBytes, anotherError) = anotherResult
                                if (anotherError != null) {
                                    val errorResult = Gson().fromJson(
                                        String(anotherResponse.data),
                                        GetError::class.java
                                    )
                                    FL.e("Fuel error", anotherError.message.toString())
                                    FL.e("Server error", errorResult.detail)
                                }
                                if (moreBytes != null) {
                                    val infoFile = File(filesDir, "${profileFilename}Info.json")
                                    infoFile.writeText(String(moreBytes))
                                }
                            }

                            val downloadProfileURL = "${profilesUrl}${latestProfile.id}/file"

                            thread(start = true) {
                                val yetAnotherResult = Fuel.get(downloadProfileURL)
                                    .authentication()
                                    .bearer(key)
                                    .download()
                                    .fileDestination { yetAnotherResponse, url ->
                                        if (yetAnotherResponse.statusCode == 200) {
                                            File(filesDir, "${profileFilename}.joblib")
                                        } else File(filesDir, "${profileFilename}.error")
                                    }
                                    .response()

                                if (yetAnotherResult.component2() == null) {
                                    FL.i("Continuous profile updated")
                                }
                            }
                        } else return@thread
                    }
                }
            }
        } finally {
            mutex.unlock()
        }
    }
}