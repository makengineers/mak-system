package com.service.safeservice.Common

class CyclicArray<T>(maxSize: Int) : Iterable<T> {

    val size: Int get() = realSize
    fun push(v: T)
    {
        if(size < maxSize) {
            storage.add(v)
            realSize += 1
        }
        else
        {
            storage[startInd] = v
            startInd = nextInd(startInd)
        }
    }
    fun get(ind: Int) : T {
        val realInd: Int = getRealInd(ind)
        return storage[realInd]
    }

    fun set(ind: Int, v: T) {
        val realInd: Int = getRealInd(ind)
        storage[realInd] = v
    }

    fun getLast() : T {
        return get(size - 1)
    }

    override fun iterator(): Iterator<T> {
        return CyclicArrayIterator<T>(this, startInd)//To change body of created functions use File | Settings | File Templates.
    }


    fun getRange(start: Int, end: Int) : ArrayList<T> {
        if (end < start) {
            return ArrayList<T>(0)
        }
        val realStartInd: Int = getRealInd(start)
        val realEndInd: Int = if(end == size) getRealInd(0) else getRealInd(end)

        var result: ArrayList<T> = ArrayList(end - start)
        var i: Int = realStartInd
        val realSizeEnd = if (realEndInd > i) realEndInd else size
        while (i < realSizeEnd) {
            result.add(storage[i])
            i += 1
        }
        if (i == realEndInd)
            return result
        i = 0
        while (i < realEndInd) {
            result.add(storage[i])
            i += 1
        }
        return result
    }

    private class CyclicArrayIterator<out T>(array: CyclicArray<T>, start: Int) : Iterator<T> {
        override fun hasNext(): Boolean {
            val nInd = arrayRef.nextInd(actInd)
            return nInd != startInd && nInd != arrayRef.size
        }

        override fun next(): T {
            val nInd = arrayRef.nextInd(actInd)
            val r = arrayRef.storage[actInd]
            actInd = nInd
            return r
        }

        private val arrayRef: CyclicArray<T> = array
        private val startInd: Int = start
        private var actInd: Int = start

    }

    private var storage: ArrayList<T> = ArrayList(maxSize)
    private var realSize: Int = 0
    private var startInd: Int = 0
    private val maxSize: Int = maxSize
    private fun getRealInd(ind: Int) : Int {
        if(ind >= size || ind < 0)
        {
            throw IndexOutOfBoundsException()
        }
        val realInd = (startInd + ind) % maxSize
        return realInd
    }
    private fun nextInd(ind: Int) : Int {
        return (ind + 1) % maxSize
    }
}