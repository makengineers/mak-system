package com.service.safeservice.ContinuousAuth

import android.content.Context
import android.util.Log
import com.bosphere.filelogger.FL
import com.google.gson.Gson
import com.service.mak_system.ProfileInfo
import com.service.safeservice.Common.AndroidBehavioralProfile
import com.service.safeservice.Common.SensorData
import com.service.safeservice.CommonAbstracts.ApiRequester
import com.service.safeservice.CommonAbstracts.NetworkUtils
import com.service.safeservice.CommonAbstracts.SensorAuthProvider
import com.service.safeservice.CommonAbstracts.UserConfigProvider
import java.io.File
import java.io.FileNotFoundException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AndroidContinuousAuthProvider(private val filesDir: File, private val requester: ApiRequester,
                                    private val networkUtils: NetworkUtils,
                                    private val userConfigProvider: UserConfigProvider,
                                    private val interval: Long,
                                    private val profileFilename: String)
    : SensorAuthProvider {
    private var profile : AndroidBehavioralProfile? = null
    private lateinit var profileInfo : ProfileInfo
    private lateinit var timer : Timer
    private val allowMobile : Boolean get() {
        val config = userConfigProvider.GetCurrentUserConfig()
        return config?.enableMobileDataUsage ?: false
    }

    private val dateFormat : DateFormat

    init {
        dateFormat = try {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX")
        } catch (e : IllegalArgumentException) {
            FL.e(e.message)
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ")
        }
    }

    override fun Run() {
        try {
            loadProfile()
        }
        catch(e: FileNotFoundException){
            Log.i("AUTH-PROVIDER","File with continuous profile info doesn't exist")
        }

        timer = Timer()
        val task = object : TimerTask() {
            override fun run() {
                requestProfile()
            }
        }
        timer.schedule(task, 30000, this.interval)
    }

    private fun requestProfile() {

        if(!canSend())
            return

        requester.RequestContinuousAuthProfile(profile?.creationDate, profileFilename)

        loadProfile()
    }

    private fun canSend() : Boolean {
        val mobileAvailable: Boolean = networkUtils.isMobileConnected()
        val wifiAvailable: Boolean = networkUtils.isWifiConnected()
        return wifiAvailable || (mobileAvailable && allowMobile)
    }

    private fun loadProfile() {
        val infoFile = File(filesDir, "${profileFilename}Info.json")
        if(infoFile.exists() && infoFile.canRead()) {
            profileInfo = Gson().fromJson(infoFile.readText(), ProfileInfo::class.java)
            val creationDate = dateFormat.parse(profileInfo.creation_date as String) as Date
            val profileFile = File(filesDir, "${profileFilename}.joblib")
            if(profileFile.exists() && profileFile.canRead()) {
                profile = AndroidBehavioralProfile(profileFile.path, creationDate)
            }
        }
    }

    override fun Authorize(data: List<SensorData>): Boolean? {
        val config = userConfigProvider.GetCurrentUserConfig()
        if(profile == null) {
            loadProfile()
        }
        return if(profile == null || config == null || !config.enableLock) {
            null
        } else {
            val probability = profile?.PredictProbability(data) ?: return null
            return when (config.algorithmSensitivity) {
                "Low" -> {
                    probability >= 0.3F
                }
                "Medium" -> {
                    probability >= 0.5F
                }
                "High" -> {
                    probability >= 0.7F
                }
                else -> null
            }
        }
    }
}