package com.service.safeservice.CommonAbstracts

interface EventDataSaver {
    fun GetChunk() : EventDataChunk
    fun GetChunkByFile() : EventDataChunk
    fun IsChunkAvailable() : Boolean
}