package com.service.safeservice.Common

import android.annotation.TargetApi
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import com.service.safeservice.CommonAbstracts.NetworkUtils

class  AndroidNetworkUtils(private val context: Context) : NetworkUtils {
    override fun isConnected(): Boolean {
        val connMgr =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun isWifiConnected(): Boolean {
        return isConnected(ConnectivityManager.TYPE_WIFI)
    }

    override fun isMobileConnected(): Boolean {
        return isConnected(ConnectivityManager.TYPE_MOBILE)
    }

    override fun isConnected(type: Int): Boolean {
        val connMgr =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            val networkInfo = connMgr.getNetworkInfo(type)
            return networkInfo != null && networkInfo.isConnected
        } else {
            return isConnectedPrivate(type)
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun isConnectedPrivate(type: Int): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networks = connMgr.allNetworks
        var networkInfo: NetworkInfo?
        for (mNetwork in networks) {
            networkInfo = connMgr.getNetworkInfo(mNetwork)
            if (networkInfo != null && networkInfo.type == type && networkInfo.isConnected) {
                return true
            }
        }
        return false
    }
}