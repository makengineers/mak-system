package com.service.safeservice.CommonAbstracts

import com.service.safeservice.Configs.UserConfig

interface UserConfigProvider {
    fun GetCurrentUserConfig(): UserConfig?
}