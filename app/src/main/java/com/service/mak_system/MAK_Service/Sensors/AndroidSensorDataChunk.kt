package com.service.safeservice.Sensors

import com.service.safeservice.CommonAbstracts.SensorDataChunk
import java.util.*

class AndroidSensorRawDataChunk(private val data: ByteArray, private val date: Date) : SensorDataChunk {
    override fun GetStartDate(): Date {
        return date
    }

    override fun GetData(): ByteArray {
        return data
    }
}

class AndroidSensorFileDataChunk(filename: String, private val date: Date) : SensorDataChunk {
    override fun GetStartDate(): Date {
        return date
    }

    private val data: ByteArray = filename.toByteArray()
    override fun GetData(): ByteArray {
        return data
    }
}