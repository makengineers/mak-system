package com.service.mak_system.MAK_Service.Architecture

import com.chaquo.python.PyObject
import com.chaquo.python.Python
import com.service.mak_system.MAK_Service.Common.SystemArchitecture
import com.service.mak_system.MAK_Service.CommonAbstracts.ArchitectureHelper
import java.lang.Exception

class ChaquopyArchitectureHelper : ArchitectureHelper
{
    private val python_module_name = "architecture_helper"
    private val python_func_name = "current_architecture"
    private var systemArchitecture: SystemArchitecture? = null

    private fun rawGetArchitecture(): SystemArchitecture {
        var architectureHelperPython: PyObject
        try {
            architectureHelperPython = Python.getInstance().getModule(python_module_name)
        } catch (e: Exception){
            com.bosphere.filelogger.FL.e(e, "Failed to run python module $python_module_name")
            return SystemArchitecture.UNKNOWN
        }
        try {
            val arch_string = architectureHelperPython.callAttr(python_func_name).toString()
            return if (arch_string == "BIT64")
                SystemArchitecture.BIT64
            else
                SystemArchitecture.BIT32
        } catch (e: Exception){
            com.bosphere.filelogger.FL.e(e, "Failed to run function $python_func_name in module $python_module_name")
            return SystemArchitecture.UNKNOWN
        }
    }

    override fun GetPythonArchitecture(): SystemArchitecture {
        var arch = systemArchitecture
        if (arch == null)
        {
            arch = rawGetArchitecture()
            systemArchitecture = arch
        }
        return arch
    }
}