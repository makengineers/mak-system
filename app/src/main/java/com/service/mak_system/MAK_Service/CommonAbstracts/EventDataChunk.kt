package com.service.safeservice.CommonAbstracts

import java.util.*

interface EventDataChunk {
    fun GetData() : ByteArray
    fun GetStartDate() : Date
}