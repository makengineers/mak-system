package com.service.safeservice.CommonAbstracts

import com.service.safeservice.Common.SensorData

interface BehavioralProfile {
    fun PredictProbability(sensorDataList: List<SensorData>) : Float?
}