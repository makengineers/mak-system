package com.service.safeservice.CommonAbstracts

import android.hardware.Sensor
import android.hardware.SensorEvent
import com.service.safeservice.Common.SensorData

abstract class SensorDataGatherer : Observable<SensorData>() {

    abstract fun ProcessAccuracyChange(sensor: Sensor?, accuracy: Int)

    abstract fun ProcessSensorEvent(event: SensorEvent?)

    abstract fun GetLastEventsInNumber(size: Int): List<SensorData>

    abstract fun GetLastEventsInTime(delayms: Int): List<SensorData>

    abstract fun GetLastEvent(): SensorData
}