package com.service.safeservice.CommonAbstracts

import java.util.*

interface SensorDataChunk {
    fun GetData() : ByteArray
    fun GetStartDate() : Date
}