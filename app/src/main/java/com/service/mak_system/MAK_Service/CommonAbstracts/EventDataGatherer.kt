package com.service.safeservice.CommonAbstracts

import android.content.Context
import android.content.Intent
import com.service.safeservice.Common.SystemEvent

abstract class EventDataGatherer : Observable<SystemEvent>() {

    abstract fun ProcessEvent(context: Context?, intent: Intent?)
}