package com.service.safeservice.Events

import com.service.safeservice.CommonAbstracts.EventDataChunk
import java.util.*

class AndroidEventRawDataChunk(private val data: ByteArray, private val date: Date) : EventDataChunk {
    override fun GetStartDate(): Date {
        return date
    }

    override fun GetData(): ByteArray {
        return data
    }
}

class AndroidEventFileDataChunk(filename: String, private val date: Date) : EventDataChunk {
    override fun GetStartDate(): Date {
        return date
    }

    private val data: ByteArray = filename.toByteArray()
    override fun GetData(): ByteArray {
        return data
    }
}