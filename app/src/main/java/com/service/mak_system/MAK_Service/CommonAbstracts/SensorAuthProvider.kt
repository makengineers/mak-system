package com.service.safeservice.CommonAbstracts

import com.service.safeservice.Common.SensorData

interface SensorAuthProvider {
    fun Run()
    fun Authorize(data: List<SensorData>): Boolean?
}