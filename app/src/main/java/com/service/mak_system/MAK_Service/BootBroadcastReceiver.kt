package com.service.safeservice

import android.app.PendingIntent.CanceledException
import android.app.PendingIntent.getActivity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.service.safeservice.Configs.AndroidUserConfigProvider

class BootBroadcastReceiver : BroadcastReceiver(), ScreenLockRunner {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action.equals(Intent.ACTION_BOOT_COMPLETED)) {

            val userConfigProvider = AndroidUserConfigProvider(context.filesDir,
                context.resources.getString(R.string.user_config_filename))

            runScreenLock(context, userConfigProvider)

            val serviceIntent = Intent(context, ServiceStarter::class.java)

            val pendingIntent = getActivity(context, 0, serviceIntent, 0)
            try {
                pendingIntent.send()
            }
            catch (e: CanceledException) {
                e.printStackTrace()
            }
        }
    }

}