package com.service.safeservice

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.bosphere.filelogger.FL

class ServiceStarter : Activity() {
    var mServiceIntent: Intent? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            mServiceIntent = Intent(this, MainService::class.java)
            if (!isMyServiceRunning(MainService::class.java)) {
                startService(mServiceIntent)
            }

            val intent = Intent(this, MainService::class.java)
            startService(intent)
            finish()
        }
        catch(e: Exception){
            FL.e("MainActivity failed: " + e.message)
            throw e
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }
}