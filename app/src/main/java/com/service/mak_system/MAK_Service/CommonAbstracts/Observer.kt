package com.service.safeservice.CommonAbstracts

import java.lang.ref.WeakReference

interface Observer<T: Any> {
    fun Update(data: T)
}

abstract class Observable<T: Any> {
    var observers = mutableListOf<WeakReference<Observer<T>>>()

    fun Subscribe(observer: Observer<T>) {
        observers.add(WeakReference(observer))
    }

    fun Unsubscribe(observer: Observer<T>) {
        var activeObservers = mutableListOf<WeakReference<Observer<T>>>()
        for (obs in observers) {
            obs.get()?.let {
                if(it != observer)
                {
                    activeObservers.add(WeakReference(it))
                }
            }
        }
        observers = activeObservers

    }

    protected fun Notify(data: T)
    {
        var activeObservers = mutableListOf<WeakReference<Observer<T>>>()
        for (observer in observers) {
            observer.get()?.let {
                activeObservers.add(WeakReference(it))
                it.Update(data)
            }
        }
    }
}