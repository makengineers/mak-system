package com.service.safeservice.Common

import com.service.safeservice.CommonAbstracts.FileSystemManager
import java.io.File
import java.io.FileOutputStream

class AndroidInternalFileSystemManager(filesDir: File) : FileSystemManager(filesDir) {

    private var workingDirectory = filesDir

    override fun OpenFile(filename: String): FileOutputStream {
        val file = File(workingDirectory, filename)
        return FileOutputStream(file)
    }

    override fun LoadFile(filename: String): File {
        return File(workingDirectory, filename)
    }

    override fun ListFiles(): MutableList<File> {
        val fileList : MutableList<File> = mutableListOf()
        if(workingDirectory.listFiles()!=null) {
            workingDirectory.walkTopDown().forEach {
                run {
                    if(it.isFile)
                        fileList.add(it)
                }
            }
        }
        return fileList
    }

    override fun DeleteFile(filename: String) : Boolean {
        val file = File(workingDirectory, filename)
        return file.delete()
    }

    override fun CreateDirectory(name: String): Boolean {
        val sensorDir = File(workingDirectory, name)
        if(!sensorDir.exists()) {
            return sensorDir.mkdir()
        }
        return true
    }

    override fun ChangeWorkingDirectory(name: String): Boolean {
        if(File(workingDirectory, name).exists()) {
            workingDirectory = File(workingDirectory, name)
            return true
        }
        return false
    }

    override fun ContainsFile(filename: String): Boolean {
        if(workingDirectory.listFiles() != null && (workingDirectory.listFiles()?.isNotEmpty() == true)) {
            val files =  workingDirectory.listFiles()
            files?.forEach {
                run {
                    if(it.name == filename)
                        return true
                }
            }
        }
        return false
    }


}