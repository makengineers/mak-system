package com.service.safeservice.Common

import java.lang.RuntimeException

class ByteHelper {
    companion object {
        @kotlin.ExperimentalUnsignedTypes
        fun ToUByteArrayLE(x : Int) : UByteArray {
            var res: UByteArray = UByteArray(4)
            res[3] = x.ushr(24).toUByte()
            res[2] = (x.ushr(16) % 256).toUByte()
            res[1] = (x.ushr(8) % 256).toUByte()
            res[0] = (x.ushr(0) % 256).toUByte()
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun WriteAsUByteArrayInLE(x : Int, array : UByteArray, startPos : Int) {
            array[startPos + 3] = x.ushr(24).toUByte()
            array[startPos + 2] = (x.ushr(16) % 256).toUByte()
            array[startPos + 1] = (x.ushr(8) % 256).toUByte()
            array[startPos + 0] = (x.ushr(0) % 256).toUByte()
        }

        @kotlin.ExperimentalUnsignedTypes
        fun ToUByteArrayLE(x : Long) : UByteArray {
            var res: UByteArray = UByteArray(8)
            res[7] = x.ushr(56).toUByte()
            res[6] = (x.ushr(48) % 256).toUByte()
            res[5] = (x.ushr(40) % 256).toUByte()
            res[4] = (x.ushr(32) % 256).toUByte()
            res[3] = (x.ushr(24) % 256).toUByte()
            res[2] = (x.ushr(16) % 256).toUByte()
            res[1] = (x.ushr(8) % 256).toUByte()
            res[0] = (x.ushr(0) % 256).toUByte()
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun WriteAsUByteArrayInLE(x : Long, array : UByteArray, startPos : Int) {
            array[startPos + 7] = x.ushr(56).toUByte()
            array[startPos + 6] = (x.ushr(48) % 256).toUByte()
            array[startPos + 5] = (x.ushr(40) % 256).toUByte()
            array[startPos + 4] = (x.ushr(32) % 256).toUByte()
            array[startPos + 3] = (x.ushr(24) % 256).toUByte()
            array[startPos + 2] = (x.ushr(16) % 256).toUByte()
            array[startPos + 1] = (x.ushr(8) % 256).toUByte()
            array[startPos + 0] = (x.ushr(0) % 256).toUByte()
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToIntLE(arr : UByteArray) : Int {
            if(arr.size != 4) {
                throw RuntimeException("Failed to convert byte array - expected array of size 4")
            }
            var res : Int = 0
            val sign : Int = if(arr[3].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1 shl 24) * (arr[3].toInt() % 128)
            res += (1 shl 16) * arr[2].toInt()
            res += (1 shl 8) * arr[1].toInt()
            res += arr[0].toInt()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToIntLE(arr : UByteArray, startPos: Int) : Int {
            var res : Int = 0
            val sign : Int = if(arr[startPos + 3].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1 shl 24) * (arr[startPos + 3].toInt() % 128)
            res += (1 shl 16) * arr[startPos + 2].toInt()
            res += (1 shl 8) * arr[startPos + 1].toInt()
            res += arr[startPos].toInt()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToLongLE(arr : UByteArray) : Long {
            if(arr.size != 4) {
                throw RuntimeException("Failed to convert byte array - expected array of size 4")
            }
            var res : Long = 0
            val sign : Int = if(arr[7].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1L shl 56) * (arr[7].toLong() % 128)
            res += (1L shl 48) * arr[6].toLong()
            res += (1L shl 40) * arr[5].toLong()
            res += (1L shl 32) * arr[4].toLong()
            res += (1L shl 24) * arr[3].toLong()
            res += (1L shl 16) * arr[2].toLong()
            res += (1L shl 8) * arr[1].toLong()
            res += arr[0].toLong()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToLongLE(arr : UByteArray, startPos: Int) : Long {
            var res : Long = 0
            val sign : Int = if(arr[startPos + 7].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1L shl 56) * (arr[startPos + 7].toLong() % 128)
            res += (1L shl 48) * arr[startPos + 6].toLong()
            res += (1L shl 40) * arr[startPos + 5].toLong()
            res += (1L shl 32) * arr[startPos + 4].toLong()
            res += (1L shl 24) * arr[startPos + 3].toLong()
            res += (1L shl 16) * arr[startPos + 2].toLong()
            res += (1L shl 8) * arr[startPos + 1].toLong()
            res += arr[startPos].toLong()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun ToUByteArrayBE(x : Int) : UByteArray {
            var res: UByteArray = UByteArray(4)
            res[0] = x.ushr(24).toUByte()
            res[1] = (x.ushr(16) % 256).toUByte()
            res[2] = (x.ushr(8) % 256).toUByte()
            res[3] = (x.ushr(0) % 256).toUByte()
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun WriteAsUByteArrayInBE(x : Int, array : UByteArray, startPos : Int) {
            array[startPos + 0] = x.ushr(24).toUByte()
            array[startPos + 1] = (x.ushr(16) % 256).toUByte()
            array[startPos + 2] = (x.ushr(8) % 256).toUByte()
            array[startPos + 3] = (x.ushr(0) % 256).toUByte()
        }

        @kotlin.ExperimentalUnsignedTypes
        fun ToUByteArrayBE(x : Long) : UByteArray {
            var res: UByteArray = UByteArray(8)
            res[0] = x.ushr(56).toUByte()
            res[1] = (x.ushr(48) % 256).toUByte()
            res[2] = (x.ushr(40) % 256).toUByte()
            res[3] = (x.ushr(32) % 256).toUByte()
            res[4] = (x.ushr(24) % 256).toUByte()
            res[5] = (x.ushr(16) % 256).toUByte()
            res[6] = (x.ushr(8) % 256).toUByte()
            res[7] = (x.ushr(0) % 256).toUByte()
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun WriteAsUByteArrayInBE(x : Long, array : UByteArray, startPos : Int) {
            array[startPos + 0] = x.ushr(56).toUByte()
            array[startPos + 1] = (x.ushr(48) % 256).toUByte()
            array[startPos + 2] = (x.ushr(40) % 256).toUByte()
            array[startPos + 3] = (x.ushr(32) % 256).toUByte()
            array[startPos + 4] = (x.ushr(24) % 256).toUByte()
            array[startPos + 5] = (x.ushr(16) % 256).toUByte()
            array[startPos + 6] = (x.ushr(8) % 256).toUByte()
            array[startPos + 7] = (x.ushr(0) % 256).toUByte()
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToIntBE(arr : UByteArray) : Int {
            if(arr.size != 4) {
                throw RuntimeException("Failed to convert byte array - expected array of size 4")
            }
            var res : Int = 0
            val sign : Int = if(arr[0].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1 shl 24) * (arr[0].toInt() % 128)
            res += (1 shl 16) * arr[1].toInt()
            res += (1 shl 8) * arr[2].toInt()
            res += arr[3].toInt()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToIntBE(arr : UByteArray, startPos: Int) : Int {
            var res : Int = 0
            val sign : Int = if(arr[startPos].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1 shl 24) * (arr[startPos].toInt() % 128)
            res += (1 shl 16) * arr[startPos + 1].toInt()
            res += (1 shl 8) * arr[startPos + 2].toInt()
            res += arr[startPos + 3].toInt()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToLongBE(arr : UByteArray) : Long {
            if(arr.size != 4) {
                throw RuntimeException("Failed to convert byte array - expected array of size 4")
            }
            var res : Long = 0
            val sign : Int = if(arr[0].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1L shl 56) * (arr[0].toLong() % 128)
            res += (1L shl 48) * arr[1].toLong()
            res += (1L shl 40) * arr[2].toLong()
            res += (1L shl 32) * arr[3].toLong()
            res += (1L shl 24) * arr[4].toLong()
            res += (1L shl 16) * arr[5].toLong()
            res += (1L shl 8) * arr[6].toLong()
            res += arr[7].toLong()
            res *= sign
            return res
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromUByteArrayToLongBE(arr : UByteArray, startPos: Int) : Long {
            var res : Long = 0
            val sign : Int = if(arr[startPos].toInt() and (1 shl 3) != 0) 1 else -1
            res += (1L shl 56) * (arr[startPos].toLong() % 128)
            res += (1L shl 48) * arr[startPos + 1].toLong()
            res += (1L shl 40) * arr[startPos + 2].toLong()
            res += (1L shl 32) * arr[startPos + 3].toLong()
            res += (1L shl 24) * arr[startPos + 4].toLong()
            res += (1L shl 16) * arr[startPos + 5].toLong()
            res += (1L shl 8) * arr[startPos + 6].toLong()
            res += arr[startPos + 7].toLong()
            res *= sign
            return res
        }
    }
}