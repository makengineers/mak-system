package com.service.safeservice.CommonAbstracts

interface SensorDataSaver {
    fun GetChunk() : SensorDataChunk
    fun GetChunkByFile() : SensorDataChunk
    fun IsChunkAvailable() : Boolean
}