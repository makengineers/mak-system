package com.service.safeservice.Events

import android.content.Context
import android.content.Intent
import com.service.safeservice.Common.SystemEvent
import com.service.safeservice.CommonAbstracts.EventDataGatherer

class AndroidEventDataGatherer : EventDataGatherer() {

    override fun ProcessEvent(context: Context?, intent: Intent?) {
        val timestamp = System.currentTimeMillis()
        val type = SystemEvent.ConvertToSystemEventType(intent!!.action!!)
        val systemEvent = SystemEvent(
            timestamp,
            type,
            IntArray(0)
        )
        Notify(systemEvent)
    }
}