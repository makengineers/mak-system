package com.service.safeservice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.service.safeservice.Configs.AndroidUserConfigProvider


class ScreenOffBroadcastReceiver : BroadcastReceiver(), ScreenLockRunner {
    override fun onReceive(context: Context, intent: Intent?) {
        if(intent?.action.equals(Intent.ACTION_SCREEN_OFF)) {
            val userConfigProvider = AndroidUserConfigProvider(context.filesDir,
                context.resources.getString(R.string.user_config_filename))
            runScreenLock(context, userConfigProvider)
        }
    }
}
