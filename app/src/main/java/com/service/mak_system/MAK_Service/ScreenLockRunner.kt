package com.service.safeservice

import android.content.Context
import android.content.Intent
import com.service.safeservice.CommonAbstracts.UserConfigProvider

interface ScreenLockRunner {
    fun runScreenLock(context: Context, userConfigProvider: UserConfigProvider) {
        val config = userConfigProvider.GetCurrentUserConfig()
        if(config != null) {
            val pinHash = config.screenLockPINHash
            val intent = Intent("com.screenlock.safelock.ScreenLock")
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
            intent.putExtra("screenLockPINHash", pinHash)
            intent.putExtra("useGesture", config.useGesture)
            context.startActivity(intent)
        }
    }
}