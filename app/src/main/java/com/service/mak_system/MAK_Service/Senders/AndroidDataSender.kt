package com.service.safeservice.Senders

import android.content.Context
import com.bosphere.filelogger.FL
import com.service.safeservice.CommonAbstracts.*
import java.io.File
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*

class AndroidDataSender(private val sensorSaver: SensorDataSaver, private val eventSaver: EventDataSaver,
                        private val requester: ApiRequester, private val networkUtils: NetworkUtils,
                        private val userConfigProvider: UserConfigProvider, private val context: Context,
                        private val interval: Long) : DataSender
{
    private lateinit var timer : Timer
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private val allowMobile : Boolean get() {
        val config = userConfigProvider.GetCurrentUserConfig()
        return config?.enableMobileDataUsage ?: false
    }

    override fun Run() {
        timer = Timer()
        val task = object : TimerTask() {
            override fun run() {
                if(!canSend())
                    return

                if(sensorSaver.IsChunkAvailable() && eventSaver.IsChunkAvailable())
                {
                    val sensorChunk = sensorSaver.GetChunkByFile()
                    val sensorsFilename: String = sensorChunk.GetData().toString(Charset.defaultCharset())
                    val sensorDate: Date = sensorChunk.GetStartDate()

                    val sensorsFile = File(File(context.filesDir, "SensorData"), sensorsFilename)
                    sensorsFile.appendBytes(byteArrayOf(0,0,0,0,0,0,0,0))

                    val eventChunk = eventSaver.GetChunkByFile()
                    val eventsFilename = eventChunk.GetData().toString(Charset.defaultCharset())
                    val eventDate: Date = eventChunk.GetStartDate()

                    val eventFile = File(File(context.filesDir, "EventData"), eventsFilename)
                    eventFile.appendBytes(byteArrayOf(0,0,0,0,0,0,0,0))

                    val fileDeleter = { f: File -> f.delete() }
                    requester.SendBinaryData(sensorsFilename, eventsFilename, dateFormat.format(sensorDate),
                        dateFormat.format(eventDate), fileDeleter)
                }
                else {
                    FL.i("Could not send data: no chunk available")
                }
            }
        }
        timer.schedule(task, this.interval, this.interval)
    }

    private fun canSend() : Boolean {
        val mobileAvailable: Boolean = networkUtils.isMobileConnected()
        val wifiAvailable: Boolean = networkUtils.isWifiConnected()

        FL.i("Network check:\n\tmobile data available: $mobileAvailable\n\twifi available: $wifiAvailable\n\tuser allows mobile data usage: $allowMobile")

        return wifiAvailable || (mobileAvailable && allowMobile)
    }

}