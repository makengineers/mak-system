package com.service.safeservice.Common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.lang.RuntimeException

@Parcelize
class SensorData(var timestamp: Long, var acceleration: FloatArray, var magneticField: FloatArray, var gyroscope: FloatArray,
                 var gravity: FloatArray, var linearAcceleration: FloatArray, var rotation: FloatArray) : Parcelable {

    fun totalSize() : Int {
        var size = 8 + acceleration.size * 4 + magneticField.size * 4 + gyroscope.size * 4 + gravity.size *4 + linearAcceleration.size * 4 + rotation.size * 4
        return size
    }

    fun copy(): SensorData
    {
        return SensorData(timestamp, acceleration, magneticField, gyroscope, gravity, linearAcceleration, rotation)
    }

    fun ToVector(): FloatArray {
        var result = FloatArray(
            acceleration.size + magneticField.size + gyroscope.size + gravity.size
                    + linearAcceleration.size + rotation.size
        )
        var actSize = 0
        System.arraycopy(acceleration, 0, result, actSize, acceleration.size)
        actSize += acceleration.size
        System.arraycopy(magneticField, 0, result, actSize, magneticField.size)
        actSize += magneticField.size
        System.arraycopy(gyroscope, 0, result, actSize, gyroscope.size)
        actSize += gyroscope.size
        System.arraycopy(gravity, 0, result, actSize, gravity.size)
        actSize += gravity.size
        System.arraycopy(linearAcceleration, 0, result, actSize, linearAcceleration.size)
        actSize += linearAcceleration.size
        System.arraycopy(rotation, 0, result, actSize, rotation.size)
        actSize += rotation.size
        return result
    }

    companion object {
        private val sensorSize = 3
        fun FromVector(timestamp: Long, vector: FloatArray) : SensorData {
            var acceleration = FloatArray(sensorSize)
            var magneticField = FloatArray(sensorSize)
            var gyroscope = FloatArray(sensorSize)
            var gravity = FloatArray(sensorSize)
            var linearAcceleration = FloatArray(sensorSize)
            var rotation = FloatArray(sensorSize)
            var actSize = 0
            System.arraycopy(vector, actSize, acceleration, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, magneticField, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, gyroscope, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, gravity, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, linearAcceleration, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, rotation, 0,
                sensorSize
            )
            actSize += sensorSize
            return SensorData(
                timestamp, acceleration, magneticField, gyroscope, gravity, linearAcceleration,
                rotation
            )
        }
    }
}
