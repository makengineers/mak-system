package com.service.safeservice.Common

import com.bosphere.filelogger.FL
import com.chaquo.python.Python
import com.google.gson.Gson
import com.service.safeservice.CommonAbstracts.BehavioralProfile
import java.lang.Exception
import java.util.*

class AndroidBehavioralProfile(private val profilePath : String, val creationDate : Date) : BehavioralProfile {
    override fun PredictProbability(data: List<SensorData>) : Float? {
        val converted = mutableListOf<Pair<Long, List<Float>>>()

        for (it in data) {
            converted.add(Pair(it.timestamp, it.ToVector().toList()))
        }
        val list = mutableListOf<List<Any>>()
        for (it in converted) {
            val l = mutableListOf<Any>()
            l.add(it.first)
            for (it2 in it.second) {
                l.add(it2)
            }
            list.add(l)
        }
        val gson = Gson()
        val json = gson.toJson(list)
        val pyObjectMLService = Python.getInstance().getModule("ml_service")
        return try {
            pyObjectMLService.callAttr("predict_probability", json, profilePath).toFloat()
        }
        catch (e : Exception){
            FL.e("Chaquopy error", e.message)
            null
        }
    }
}