package com.service.safeservice.Sensors

import com.service.safeservice.Common.ByteHelper
import com.service.safeservice.Common.SensorData
import com.service.safeservice.CommonAbstracts.SensorDataConverter
import java.lang.RuntimeException

class AndroidSensorDataConverter(private val accelerationMax: Float,
                                 private val magneticFieldMax: Float,
                                 private val gyroscopeMax: Float,
                                 private val gravityMax: Float,
                                 private val linearAccelerationMax: Float,
                                 private val rotationMax: Float) : SensorDataConverter
{
    private val expectedByteSize = 80
    private val sensorSize = 3
    private val VERSION_OFFSET: Long = 0x0800000000000000

    @kotlin.ExperimentalUnsignedTypes
    override fun ToBytes(data: SensorData) : UByteArray {
        var res = UByteArray(data.totalSize())
        val timestamp = System.currentTimeMillis() + VERSION_OFFSET
        val convertedAccX : Int = (data.acceleration[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedAccY : Int = (data.acceleration[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedAccZ : Int = (data.acceleration[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedMagX : Int = (data.magneticField[0].toDouble() * Int.MAX_VALUE / magneticFieldMax).toInt()
        val convertedMagY : Int = (data.magneticField[1].toDouble() * Int.MAX_VALUE / magneticFieldMax).toInt()
        val convertedMagZ : Int = (data.magneticField[2].toDouble() * Int.MAX_VALUE / magneticFieldMax).toInt()
        val convertedGyroX : Int = (data.gyroscope[0].toDouble() * Int.MAX_VALUE / gyroscopeMax).toInt()
        val convertedGyroY : Int = (data.gyroscope[1].toDouble() * Int.MAX_VALUE / gyroscopeMax).toInt()
        val convertedGyroZ : Int = (data.gyroscope[2].toDouble() * Int.MAX_VALUE / gyroscopeMax).toInt()
        val convertedGravX : Int = (data.gravity[0].toDouble() * Int.MAX_VALUE / gravityMax).toInt()
        val convertedGravY : Int = (data.gravity[1].toDouble() * Int.MAX_VALUE / gravityMax).toInt()
        val convertedGravZ : Int = (data.gravity[2].toDouble() * Int.MAX_VALUE / gravityMax).toInt()
        val convertedLinAccX : Int = (data.linearAcceleration[0].toDouble() * Int.MAX_VALUE / linearAccelerationMax).toInt()
        val convertedLinAccY : Int = (data.linearAcceleration[1].toDouble() * Int.MAX_VALUE / linearAccelerationMax).toInt()
        val convertedLinAccZ : Int = (data.linearAcceleration[2].toDouble() * Int.MAX_VALUE / linearAccelerationMax).toInt()
        val convertedRotX : Int = (data.rotation[0].toDouble() * Int.MAX_VALUE / rotationMax).toInt()
        val convertedRotY : Int = (data.rotation[1].toDouble() * Int.MAX_VALUE / rotationMax).toInt()
        val convertedRotZ : Int = (data.rotation[2].toDouble() * Int.MAX_VALUE / rotationMax).toInt()
        var actOffset : Int = 0
        ByteHelper.WriteAsUByteArrayInBE(timestamp, res, actOffset)
        actOffset += 8
        ByteHelper.WriteAsUByteArrayInBE(convertedAccX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedAccY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedAccZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedMagX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedMagY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedMagZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGyroX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGyroY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGyroZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGravX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGravY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGravZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedLinAccX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedLinAccY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedLinAccZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedRotX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedRotY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedRotZ, res, actOffset)
        actOffset += 4
        return res
    }

    @kotlin.ExperimentalUnsignedTypes
    override fun FromBytes(arr: UByteArray) : SensorData {
        if(arr.size != expectedByteSize) {
            throw RuntimeException("Array of size ${arr.size} was not expected (expected $expectedByteSize")
        }

        var timestamp: Long
        var acceleration: FloatArray = FloatArray(sensorSize)
        var magneticField: FloatArray = FloatArray(sensorSize)
        var gyroscope: FloatArray = FloatArray(sensorSize)
        var gravity: FloatArray = FloatArray(sensorSize)
        var linearAcceleration: FloatArray = FloatArray(sensorSize)
        var rotation: FloatArray = FloatArray(sensorSize)
        var actOffset: Int = 0
        timestamp = ByteHelper.FromUByteArrayToLongBE(arr, actOffset)
        actOffset += 8
        acceleration[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * accelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        acceleration[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * accelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        acceleration[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * accelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        magneticField[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * magneticFieldMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        magneticField[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * magneticFieldMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        magneticField[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * magneticFieldMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        gyroscope[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gyroscopeMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        gyroscope[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gyroscopeMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        gyroscope[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gyroscopeMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        gravity[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gravityMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        gravity[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gravityMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        gravity[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gravityMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        linearAcceleration[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * linearAccelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        linearAcceleration[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * linearAccelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        linearAcceleration[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * linearAccelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        rotation[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * rotationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        rotation[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * rotationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        rotation[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * rotationMax.toDouble() / Int.MAX_VALUE).toFloat()
        actOffset += 4
        return SensorData(
            timestamp,
            acceleration,
            magneticField,
            gyroscope,
            gravity,
            linearAcceleration,
            rotation
        )
    }
}