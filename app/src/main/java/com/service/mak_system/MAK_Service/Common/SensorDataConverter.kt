package com.service.safeservice.CommonAbstracts

import com.service.safeservice.Common.SensorData

interface SensorDataConverter {
    @kotlin.ExperimentalUnsignedTypes
    fun ToBytes(data: SensorData): UByteArray

    @kotlin.ExperimentalUnsignedTypes
    fun FromBytes(arr: UByteArray): SensorData
}