package com.service.safeservice.CommonAbstracts

interface NetworkUtils {
    fun isConnected(): Boolean
    fun isWifiConnected(): Boolean
    fun isMobileConnected(): Boolean
    fun isConnected(type: Int): Boolean
}