package com.service.safeservice.Sensors

import android.hardware.Sensor
import android.hardware.SensorEvent
import com.service.safeservice.Common.CyclicArray
import com.service.safeservice.Common.SensorData
import com.service.safeservice.CommonAbstracts.SensorDataGatherer
import java.util.*
import java.util.concurrent.locks.ReentrantLock

class AndroidSensorDataGatherer(interval: Float, storageSize: Int) : SensorDataGatherer() {

    var actual: SensorData = SensorData.FromVector(0, FloatArray(19))
    val interval: Long = (interval*1000).toLong()
    private val mutex = ReentrantLock()
    private val Logger = java.util.logging.Logger.getLogger(AndroidSensorDataGatherer::class.java.name)
    private var storage: CyclicArray<SensorData> = CyclicArray(storageSize)

    override fun GetLastEvent(): SensorData {
        return storage.getLast()
    }

    override fun ProcessAccuracyChange(sensor: Sensor?, accuracy: Int) {
        Logger.info("${sensor?.name} has changed accuracy to $accuracy")
    }

    override fun ProcessSensorEvent(event: SensorEvent?) {
        val vals: FloatArray = event?.values?.take(3)!!.toFloatArray()
        when(event.sensor?.type) {
            Sensor.TYPE_ACCELEROMETER -> actual.acceleration = vals
            Sensor.TYPE_MAGNETIC_FIELD -> actual.magneticField = vals
            Sensor.TYPE_GYROSCOPE -> actual.gyroscope = vals
            Sensor.TYPE_GRAVITY -> actual.gravity = vals
            Sensor.TYPE_LINEAR_ACCELERATION -> actual.linearAcceleration = vals
            Sensor.TYPE_ROTATION_VECTOR -> actual.rotation = vals
        }
    }

    override fun GetLastEventsInNumber(size: Int): List<SensorData> {
        mutex.lock()
        val res = storage.getRange(storage.size - size, storage.size)
        mutex.unlock()
        return res
    }

    override fun GetLastEventsInTime(delayms: Int): List<SensorData> {
        mutex.lock()
        val timestamp = System.currentTimeMillis()
        val start = timestamp - delayms
        var i = 0
        for (it in storage) {
            if(it.timestamp >= start)
                break
            ++i
        }
        val res = storage.getRange(i, storage.size)
        mutex.unlock()
        return res
    }

    private val timer : Timer = Timer()
    init {
        val task = object : TimerTask() {
            override fun run() {
                mutex.lock()
                actual.timestamp = System.currentTimeMillis()
                storage.push(actual.copy())
                Notify(actual.copy())
                mutex.unlock()
            }
        }
        timer.schedule(task, this.interval, this.interval)
    }
}