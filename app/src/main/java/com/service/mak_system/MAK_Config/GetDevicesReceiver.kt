package com.service.mak_system.MAK_Config

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.makconfig.MainConfigActivity
import com.google.gson.GsonBuilder
import com.service.mak_system.Device
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class GetDevicesReceiver(private val configActivity: MainConfigActivity) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val devicesString = intent!!.getStringExtra("devicesString") as String
        val key = intent.getStringExtra("key") as String

        val gson = GsonBuilder().create()
        val devices = gson.fromJson(devicesString, Array<Device>::class.java).toList()

        val file = File(configActivity.filesDir, "deviceIDs.json")

        if(devices.isEmpty()) {
            configActivity.AddDevice(key)
        }
        else {
            devices.forEach {
                if(it.android_id == configActivity.deviceID ) {
                    file.writeText(gson.toJson(it))
                    configActivity.SetLabelRunService()
                    return
                }
            }
            configActivity.SelectDevice(devicesString, key)
        }
    }

}