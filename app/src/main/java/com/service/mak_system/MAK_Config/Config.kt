package com.example.makconfig

internal data class Config(var enableUnlock: Boolean, var enableLock: Boolean,
                           var enableMobileDataUsage: Boolean, var screenLockPINHash: String?,
                           var algorithmSensitivity: String, var useGesture: Boolean)
