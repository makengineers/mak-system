package com.example.makconfig

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Resources
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bosphere.filelogger.FL
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.authentication
import com.google.gson.Gson
import com.screenlock.safelock.ConfirmPopup
import com.service.mak_system.Device
import com.service.mak_system.MAK_Config.GetDevicesReceiver
import com.service.safeservice.*
import com.service.safeservice.Common.AndroidNetworkUtils
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileReader
import kotlin.concurrent.thread


class MainConfigActivity : AppCompatActivity() {
    private val CHANGE_PIN_ACTIVITY = 1
    private val LOGGED_IN_OR_REGISTERED = 2
    private val SELECT_DEVICE = 3

    private val configFilename = "config.json"
    private lateinit var addDeviceURL : String
    private lateinit var getDevicesURL : String
    private lateinit var modifyDeviceURL : String
    private val getDevicesReceiver : GetDevicesReceiver = GetDevicesReceiver(this)
    lateinit var deviceID : String
    private var activityContext: Context? = null
    private var config = Config(
        enableUnlock = false, enableLock = false, enableMobileDataUsage = false,
        screenLockPINHash = null, algorithmSensitivity = "High", useGesture = false
    )
    private var configViewsList = mutableListOf<View?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addDeviceURL = baseContext.resources.getString(R.string.devices_url)
        getDevicesURL = baseContext.resources.getString(R.string.my_devices_url)
        modifyDeviceURL = baseContext.resources.getString(R.string.devices_url)

        setContentView(R.layout.activity_main)
        deviceID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        configViewsList = mutableListOf(
            findViewById<TextView>(R.id.yourDeviceIdLabel),
            findViewById<EditText>(R.id.deviceIdLabel),
            findViewById<Switch>(R.id.unlockSwitch),
            findViewById<Switch>(R.id.lockSwitch),
            findViewById<Switch>(R.id.mobileSwitch),
            findViewById<Switch>(R.id.gestureSwitch),
            findViewById<TextView>(R.id.sensitivityLabel),
            findViewById<Spinner>(R.id.sensitivitySpinner),
            findViewById<Button>(R.id.pinButton),
            findViewById<Button>(R.id.unlockProfileButton),
            findViewById<Button>(R.id.continuousProfileButton),
            findViewById<Button>(R.id.logoutButton))

        activityContext = this.baseContext

        loadConfig(this.baseContext)

        val filter = IntentFilter("devices_string_received")
        registerReceiver(getDevicesReceiver, filter)

        val file = File(baseContext.filesDir, "receivedKey.txt")
        if(!file.exists()){
            hideConfig()
        }
        else {
            showConfig()
            val key = file.readText()
            val devicesFile = File(baseContext.filesDir, "deviceIDs.json")
            if(!devicesFile.exists()){
                getDevices(key)
            }
            else {
                SetLabelRunService()
            }
        }

        if (config.enableUnlock)
            unlockSwitch.isChecked = true
        if (config.enableLock)
            lockSwitch.isChecked = true
        if (config.enableMobileDataUsage)
            mobileSwitch.isChecked = true
        if(config.useGesture)
            gestureSwitch.isChecked = true

        unlockSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            enableUnlock(buttonView, isChecked)
        }
        lockSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            enableLock(buttonView, isChecked)
        }
        mobileSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            enableMobileData(buttonView, isChecked)
        }
        gestureSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            useGesture(buttonView, isChecked)
        }

        val spinner: Spinner = findViewById(R.id.sensitivitySpinner)
        ArrayAdapter.createFromResource(
            this,
            R.array.sensitivity_levels_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(getIndex(spinner, config.algorithmSensitivity))
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                config.algorithmSensitivity = spinner.selectedItem as String
                saveConfig(selectedItemView.context)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
            }
        }
    }

    fun SetLabelRunService() {
        deviceIdLabel.text = deviceID
        val mServiceIntent = Intent(this, MainService::class.java)
        if (!isMyServiceRunning(MainService::class.java)) {
            startService(mServiceIntent)
        }
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(myString, ignoreCase = true)) {
                return i
            }
        }

        throw Resources.NotFoundException()
    }

    private fun enableUnlock(buttonView: CompoundButton, isChecked: Boolean) {
        config.enableUnlock = isChecked
        saveConfig(buttonView.context)
    }

    private fun enableLock(buttonView: CompoundButton, isChecked: Boolean) {
        config.enableLock = isChecked
        saveConfig(buttonView.context)
    }

    private fun enableMobileData(buttonView: CompoundButton, isChecked: Boolean) {
        config.enableMobileDataUsage = isChecked
        saveConfig(buttonView.context)
    }

    private fun useGesture(buttonView: CompoundButton, isChecked: Boolean) {
        config.useGesture = isChecked
        saveConfig(buttonView.context)
    }

    fun setPIN(view: View) {
        val intent = Intent(this, ChangePinActivity::class.java).apply {
            putExtra("pinHash", config.screenLockPINHash)
        }
        startActivityForResult(intent, CHANGE_PIN_ACTIVITY)
    }

    fun logoutClicked(view: View) {
        val yesFunction = {  logout() }
        val noFunction = { }

        val popup = ConfirmPopup(this,"Are you sure?", yesFunction, noFunction)
        popup.show()
    }

    private fun logout() {
        val keyFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.received_key_filename))
        if(keyFile.exists()){
            keyFile.delete()
        }
        val devicesFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.device_id_json_filename))
        if(devicesFile.exists()){
            devicesFile.delete()
        }
        val profileFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.normal_profile_filename)+".joblib")
        if(profileFile.exists()){
            profileFile.delete()
        }
        val profileInfoFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.normal_profile_filename)+"Info.json")
        if(profileInfoFile.exists()){
            profileInfoFile.delete()
        }
        val contProfileFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.continuous_profile_filename)+".joblib")
        if(contProfileFile.exists()){
            contProfileFile.delete()
        }
        val contProfileInfoFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.continuous_profile_filename)+"Info.json")
        if(contProfileInfoFile.exists()){
            contProfileInfoFile.delete()
        }
        val profileErrorFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.normal_profile_filename)+".error")
        if(profileErrorFile.exists()){
            profileErrorFile.delete()
        }
        val contProfileErrorFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.continuous_profile_filename)+".error")
        if(contProfileErrorFile.exists()){
            contProfileErrorFile.delete()
        }
        if(isMyServiceRunning(MainService::class.java)){
            val intent = Intent()
            intent.action = "KILL_YOURSELF"
            this.sendBroadcast(intent)
        }
        else {
            this.finishAndRemoveTask()
        }
    }

    fun selectedLogin(view: View) {
        val loginIntent = Intent(this, LoginActivity::class.java)
        this.startActivityForResult(loginIntent, LOGGED_IN_OR_REGISTERED)
    }

    fun selectedRegister(view: View){
        val registerIntent = Intent(this, RegisterActivity::class.java)
        this.startActivityForResult(registerIntent, LOGGED_IN_OR_REGISTERED)
    }

    fun unlockProfileButtonClicked(view: View){
        val infoFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.normal_profile_filename)+"Info.json")
        if(infoFile.exists()) {
            val profileFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.normal_profile_filename) + ".joblib")
            if (profileFile.exists()) {
                val profileDetailsIntent = Intent(this, ProfileDetailsActivity::class.java)
                profileDetailsIntent.putExtra("filepath", infoFile.path)
                this.startActivity(profileDetailsIntent)
                return
            }
        }
        AlertDialog.Builder(this).setMessage("Profile not created yet").show()
    }

    fun continuousProfileButtonClicked(view: View){
        val infoFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.continuous_profile_filename)+"Info.json")
        if(infoFile.exists()){
            val profileFile = File(baseContext.filesDir, baseContext.resources.getString(R.string.continuous_profile_filename) + ".joblib")
            if(profileFile.exists()) {
                val profileDetailsIntent = Intent(this, ProfileDetailsActivity::class.java)
                profileDetailsIntent.putExtra("filepath", infoFile.path)
                this.startActivity(profileDetailsIntent)
                return
            }
        }
        AlertDialog.Builder(this).setMessage("Profile not created yet").show()
    }

    private fun hideConfig(){
        val loginButton = findViewById<Button>(R.id.loginButton)
        val registerButton = findViewById<Button>(R.id.registerButton)
        loginButton.visibility = View.VISIBLE
        registerButton.visibility = View.VISIBLE

        for(view in configViewsList){
            view?.visibility = View.GONE
        }
    }

    private fun showConfig(){
        val loginButton = findViewById<Button>(R.id.loginButton)
        val registerButton = findViewById<Button>(R.id.registerButton)
        loginButton.visibility = View.GONE
        registerButton.visibility = View.GONE

        for(view in configViewsList){
            view?.visibility = View.VISIBLE
        }
    }

    private fun saveConfig(context: Context) {
        val gson = Gson()
        val jsonString = gson.toJson(config)

        val file = File(context.filesDir, configFilename)
        file.writeText(jsonString)

        val newIntent = Intent()
        newIntent.action = "CONFIG_CHANGED"
        context.sendBroadcast(newIntent)
    }

    private fun loadConfig(context: Context) {
        val gson = Gson()
        val file = File(context.filesDir, configFilename)

        if(file.exists()) {
            val reader = FileReader(file)
            config = gson.fromJson(reader, Config::class.java)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CHANGE_PIN_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                config.screenLockPINHash = data!!.getStringExtra("pinHash") as String
                activityContext?.let { saveConfig(it) }
            }
        }
        if(requestCode == LOGGED_IN_OR_REGISTERED){
            val key = data?.getStringExtra("key")
            if(key != null) {
                showConfig()
                getDevices(key)
            }
            else return
        }
        if(requestCode == SELECT_DEVICE){
            val key = data?.getStringExtra("key")
            if(key != null) {
                val id = data.getIntExtra("id", -1)
                if (id < 0) {
                    AddDevice(key)
                } else {
                    modifyDevice(key, id)
                }
            }
        }
        activityContext?.let { saveConfig(it) }
    }

    fun SelectDevice(devices : String, key : String) {
        val selectDeviceIntent = Intent(this, SelectDeviceActivity::class.java)
        selectDeviceIntent.putExtra("devicesString", devices)
        selectDeviceIntent.putExtra("key", key)
        this.startActivityForResult(selectDeviceIntent, SELECT_DEVICE)
    }

    fun AddDevice(key : String) {
        val formData : ArrayList<Pair<String, String>> = ArrayList(1)
        formData.add(Pair("android_id", deviceID))
        checkInternet()
        thread(start = true) {
            for (i in 0..10) {
                val (_, response, result) = Fuel.post(addDeviceURL, parameters = formData)
                    .authentication()
                    .bearer(key)
                    .response()

                val (bytes, _) = result
                if(response.statusCode !in 200..299){
                    FL.e("Server Error during adding device", " Status code: " + response.statusCode)
                }
                if (bytes != null) {
                    val gson = Gson()
                    val device = gson.fromJson(String(bytes), Device::class.java)
                    FL.i("Device added: id${device.id}, android_id${device.android_id}")

                    val file = File(
                        baseContext.filesDir,
                        baseContext.resources.getString(R.string.device_id_json_filename)
                    )
                    file.writeText(String(bytes))

                    SetLabelRunService()
                    runOnUiThread {
                        val mainView = window.decorView.rootView
                        mainView.invalidate()
                    }
                    return@thread
                }
            }
            runOnUiThread {
                AlertDialog.Builder(this).setMessage("Error adding device ID \n Restart application").show()
            }
        }
    }

    private fun modifyDevice(key : String, id : Int) {
        val formData : ArrayList<Pair<String, String>> = ArrayList(1)
        formData.add(Pair("android_id", deviceID))
        checkInternet()

        thread(start = true) {
            for (i in 0..10) {
                val (_, response, result) = Fuel.put("$modifyDeviceURL$id/", parameters = formData)
                    .authentication()
                    .bearer(key)
                    .response()

                val (bytes, error) = result
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                }
                if(response.statusCode !in 200..299){
                    FL.e("Server Error during modifying device id", " Status code: " + response.statusCode)
                }
                if (bytes != null) {
                    val gson = Gson()
                    val device = gson.fromJson(String(bytes), Device::class.java)
                    FL.i("Device modified: id${device.id}, android_id${device.android_id}")

                    val file = File(
                        baseContext.filesDir,
                        baseContext.resources.getString(R.string.device_id_json_filename)
                    )
                    file.writeText(String(bytes))

                    SetLabelRunService()
                    runOnUiThread {
                        val mainView = window.decorView.rootView
                        mainView.invalidate()
                    }
                    return@thread
                }

            }
            runOnUiThread {
                AlertDialog.Builder(this)
                    .setMessage("Couldn't modify device ID. \n Restart application")
                    .show()
            }
        }
    }

    private fun getDevices(key : String) {
        checkInternet()

        thread(start = true) {
            for (i in 0..10) {
                val (_, response, result ) = Fuel.get(getDevicesURL)
                    .authentication()
                    .bearer(key)
                    .response()

                val (bytes, error) = result
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                }
                if(response.statusCode !in 200..299){
                    FL.e("Server Error during getting device id", " Status code: " + response.statusCode)
                }
                if (bytes != null) {
                    FL.i("Received list of devices")

                    val resultIntent = Intent("devices_string_received")
                    resultIntent.putExtra("devicesString", String(bytes))
                    resultIntent.putExtra("key", key)
                    runOnUiThread {
                        val mainView = window.decorView.rootView
                        mainView.invalidate()
                        this.sendBroadcast(resultIntent)
                    }
                    return@thread
                }
            }
            runOnUiThread {
                AlertDialog.Builder(this)
                    .setMessage("Couldn't obtain device ID. \n Restart application")
                    .show()
            }
        }
    }

    private fun checkInternet() {
        if(!canSend()) {
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle("No internet connection!")
            alertDialog.setMessage("Enable internet connection and restart application")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
            return
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }

    private fun canSend() : Boolean {
        val network = AndroidNetworkUtils(baseContext)
        return network.isMobileConnected() || network.isWifiConnected()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(getDevicesReceiver)
    }
}
