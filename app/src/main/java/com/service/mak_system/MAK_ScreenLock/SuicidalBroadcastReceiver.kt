package com.screenlock.safelock

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class SuicidalBroadcastReceiver(private val mainActivityContext: ScreenLock) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val authResult = intent!!.getBooleanExtra("AuthenticationSucceeded", false)
        mainActivityContext.KillYourself(authResult)
    }

}