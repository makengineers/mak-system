package com.service.safeservice

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bosphere.filelogger.FL
import com.github.kittinunf.fuel.Fuel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.service.mak_system.Key
import com.service.mak_system.LoginError
import com.service.safeservice.Common.AndroidNetworkUtils
import java.io.File

class LoginActivity : AppCompatActivity() {
    private lateinit var loginURL : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        loginURL = resources.getString(R.string.login_url)
    }

    fun loginClicked(view: View) {
        val usernameEditor = findViewById<EditText>(R.id.usernameEditor)
        val username = usernameEditor.text.toString()

        val passwordEditor = findViewById<EditText>(R.id.passwordEditor)
        val password = passwordEditor.text.toString()

        val formData : ArrayList<Pair<String, String>> = ArrayList(5)
        formData.add(Pair("username", username))
        formData.add(Pair("password", password))

        if(!canSend()){
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setMessage("No internet connection!")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
            return
        }

        val progressBar = findViewById<ProgressBar>(R.id.loginProgressBar)
        progressBar.visibility = View.VISIBLE

        val resultIntent = Intent()

        Fuel.post(loginURL, parameters = formData)
            .response { _, response, result ->
                val (bytes, error) = result
                if(error != null) {
                    FL.e("Fuel error", error.message.toString())
                    val gson = GsonBuilder().create()
                    if(response.statusCode != 201 && response.statusCode != 400) {
                        runOnUiThread {
                            progressBar.visibility = View.GONE
                            AlertDialog.Builder(this).setMessage("Error").show()
                        }
                        return@response
                    }
                    val errorResult = gson.fromJson(String(response.data), LoginError::class.java)

                    runOnUiThread {
                        if(errorResult == null) {
                            AlertDialog.Builder(this).setMessage("Error").show()
                            return@runOnUiThread
                        }
                        if (errorResult.non_field_errors != null) {
                            progressBar.visibility = View.GONE
                            passwordEditor.error = errorResult.non_field_errors.toString()
                        }
                    }
                }
                if(bytes != null){
                    val gson = Gson()
                    val keyObject = gson.fromJson(String(bytes), Key::class.java)
                    val key = keyObject.key
                    FL.i("Received device key $key")
                    val file = File(baseContext.filesDir, resources.getString(R.string.received_key_filename))
                    file.writeText(key)

                    resultIntent.putExtra("key", key)

                    runOnUiThread {
                        setResult(Activity.RESULT_OK, resultIntent)
                        this.finishAndRemoveTask()
                    }
                }
            }
    }

    private fun canSend() : Boolean {
        val network = AndroidNetworkUtils(baseContext)
        return network.isMobileConnected() || network.isWifiConnected()
    }
}