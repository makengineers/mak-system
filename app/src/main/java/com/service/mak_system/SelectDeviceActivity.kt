package com.service.safeservice

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import com.screenlock.safelock.ConfirmPopup
import com.service.mak_system.Device


class SelectDeviceActivity : AppCompatActivity() {
    private lateinit var adapter : ArrayAdapter<String>
    private lateinit var devicesString: String
    private lateinit var key: String
    private lateinit var devices  : List<Device>
    private val listItems = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_device)

        devicesString = intent.getStringExtra("devicesString") as String
        key = intent.getStringExtra("key") as String

        val listView = findViewById<ListView>(R.id.deviceSelectionList)

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)

        listView.adapter = adapter

        val gson = GsonBuilder().create()
        devices = gson.fromJson(devicesString, Array<Device>::class.java).toList()

        devices.forEach {
            listItems.add(it.android_id)
        }

        adapter.notifyDataSetChanged()

        listView.onItemClickListener =
            OnItemClickListener { _, _, position, _ ->
                val value = adapter.getItem(position)

                val yesFunction = {  yesClicked(value) }
                val noFunction = {  noClicked() }

                val popup = ConfirmPopup(this,"Do you want to modify this device?", yesFunction, noFunction)
                popup.show()
            }

    }

    private fun yesClicked(device : String?) {
        val resultIntent = Intent()
        resultIntent.putExtra("key", key)
        devices.forEach {
            if(it.android_id == device){
                resultIntent.putExtra("id", it.id)
                return@forEach
            }
        }
        setResult(Activity.RESULT_OK, resultIntent)
        this.finishAndRemoveTask()
    }

    private fun noClicked() {
        return
    }

    fun addThisDevice(view: View) {
        val resultIntent = Intent()
        resultIntent.putExtra("key", key)
        setResult(Activity.RESULT_OK, resultIntent)
        this.finishAndRemoveTask()
    }
}