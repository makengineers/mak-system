import sys

def current_architecture():
    if sys.maxsize > 2**32:
        return "BIT64"
    else:
        return "BIT32"